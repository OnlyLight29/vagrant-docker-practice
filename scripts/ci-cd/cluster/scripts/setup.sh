#!/bin/bash

useradd cluster
echo "cluster" | passwd --stdin cluster
usermod -aG wheel cluster
sed -i "s/# %wheel/%wheel/" /etc/sudoers

sed -i 's/^PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
systemctl reload sshd
